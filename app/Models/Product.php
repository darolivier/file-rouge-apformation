<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
     /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'description',
        'price',
        'stock_quantity',
        'category_id'
    ];

    public function category() {
        return $this->belongsTo(Category::class);
    }
    public function carts() {
        return $this->hasMany(Cart::class);
    }
    public function orders() {
        return $this->hasMany(Order::class);
    }


    
    protected $with = ['category'];
    

}
