<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

 /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'total_price',
        'user_id'
    ];

    public function products() {
        return $this->belongsToMany(Product::class);
    }
    public function user() {
        return $this->belongsTo(User::class);
    }

    
    protected $with = ['products'];
    
}

