<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
           'description' => fake()->text(),
           'price' => fake()->numberBetween(500, 2000),
           'stock_quantity' => fake()->numberBetween(2, 500),
           'category_id' => Category::get()->pluck('id')->random(),
           'reference' => fake()->numberBetween(00000000001, 9999999999),
           'details' => fake()->text(),
           'url' => "@/assets/computers/images" . rand(1, 22) .".jpg",
           'class' => fake()->randomElement(['suggestion', 'available','mostPoptular']),
           'stars' => rand(1,5)
        ];
    }
}
