<?php

namespace Database\Seeders;

use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          
        DB::table('orders')->truncate();
        DB::table('orders')->insert([
            'id' => 1,
            'user_id' => 2,
            'date_received' => Carbon::create('2022', '01', '01'),
            'date_delivered' => Carbon::create('2022', '01', '05'),
            'delivery_city' => 'Toulouse',
            'delivery_address' => '25 allée de Viadieu',
            'complement_address' => 'apt2109',
            'total_price' => 100,
        ]);
    }
}
