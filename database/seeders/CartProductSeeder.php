<?php

namespace Database\Seeders;

use App\Models\Cart;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CartProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('cart_product')->truncate();

        $cart = Cart::findOrFail(1);
        $product1 = Product::findOrFail(1);
        $product2 = Product::findOrFail(2);

        $cart->products()->attach($product1, ['price' => $product1->price]);
        $cart->products()->attach($product2, ['price' => $product2->price]);
    }
}
