<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->truncate();
        DB::table('roles')->insert([
            'role' => 'client',
            'id' => 1,
        ]);
        DB::table('roles')->insert([
            'role' => 'employee',
            'id' => 2,
        ]);
        DB::table('roles')->insert([
            'role' => 'superAdmin',
            'id' => 3,
        ]);
    }
}
