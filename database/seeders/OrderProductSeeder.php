<?php

namespace Database\Seeders;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_product')->truncate();

        $order = Order::findOrFail(1);
        $product3 = Product::findOrFail(3);
        $product4 = Product::findOrFail(4);
       
        $order->products()->attach($product3, ['price' => $product3->price]);
        $order->products()->attach($product4, ['price' => $product4->price]);
    }
}
