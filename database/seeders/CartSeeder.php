<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CartSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      
        DB::table('carts')->truncate();
        DB::table('carts')->insert([
            'user_id' => 1,
            'id' => 1,
            'total_price' => 120
        ]);
       
    }
    
}
