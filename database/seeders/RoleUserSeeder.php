<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Role;

class RoleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role_user')->truncate();

        $roles = Role::all();

        // Populate the pivot table
        User::all()->each(function ($user) use ($roles) {
            if($user->name === 'darolivier') {
                $user->roles()->attach($roles->firstWhere('role', 'employee')->id);
            } elseif($user->name === 'superadmin') {
                $user->roles()->attach($roles->firstWhere('role', 'superAdmin')->id);
            }elseif($user->name === 'client') {
                $user->roles()->attach($roles->firstWhere('role', 'client')->id);
            } else {
                $user->roles()->attach(
                    $roles->random(rand(1, 2))->pluck('id')->toArray()
                );
            }
            
        });
    }
}