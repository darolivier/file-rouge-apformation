<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
     
      
        DB::table('users')->insert([
            'name' => 'darolivier',
            'email' => 'olivierdarrault@gmail.com',
            'password' => Hash::make('hellohello'),
           
        ]);
        DB::table('users')->insert([
            'name' => 'superadmin',
            'email' => 'darolivier@hotmail.com',
            'password' => Hash::make('hellohello'),
           
        ]);
        DB::table('users')->insert([
            'name' => 'client',
            'email' => 'olivier@hotmail.com',
            'password' => Hash::make('hellohello'),
           
        ]);
        User::factory()->count(10)->create();
    }
}
