<?php

use App\Http\Controllers\api\CartController;
use App\Http\Controllers\api\CategoryController;
use App\Http\Controllers\api\OrderController;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\RoleController;
use App\Http\Controllers\Api\UserController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Products routes
Route::get('products',[ProductController::class, 'index'])->name('products.index');

Route::get('products/{product:id}',[ProductController::class, 'show'])->name('product.show');

Route::post('products/create', [ProductController::class, 'store'])->name('product.create');

Route::put('products/{product:id}/update',  [ProductController::class, 'update'])->name('product.update');

Route::delete('products/{product:id}/delete',  [ProductController::class, 'destroy'])->name('product.delete');

// Categories routes

Route::get('categories',[CategoryController::class, 'index'])->name('category.index');

Route::get('categories/{category:id}',[CategoryController::class, 'show'])->name('category.show');

Route::post('categories/create', [CategoryController::class, 'store'])->name('category.create');

Route::put('categories/{category:id}/update',  [CategoryController::class, 'update'])->name('category.update');

Route::delete('categories/{category:id}/delete',  [CategoryController::class, 'destroy'])->name('category.delete');

//Roles routes

Route::get('roles',[RoleController::class, 'index'])->name('role.index');

Route::get('roles/{role:id}',[RoleController::class, 'show'])->name('role.show');

Route::post('roles/create', [RoleController::class, 'store'])->name('role.create');

Route::put('roles/{role:id}/update',  [RoleController::class, 'update'])->name('role.update');

Route::delete('roles/{role:id}/delete',  [RoleController::class, 'destroy'])->name('role.delete');

//Users routes

Route::get('users',[UserController::class, 'index'])->name('user.index');

Route::get('users/{user:id}',[UserController::class, 'show'])->name('user.show');

Route::post('users/create', [UserController::class, 'store'])->name('user.create');

Route::put('users/{user:id}/update',  [UserController::class, 'update'])->name('user.update');

Route::delete('users/{user:id}/delete',  [UserController::class, 'destroy'])->name('user.delete');


//Carts routes

Route::get('carts',[CartController::class, 'index'])->name('cart.index');

Route::get('carts/{cart:id}',[CartController::class, 'show'])->name('cart.show');

Route::post('carts/create', [CartController::class, 'store'])->name('cart.create');

Route::put('carts/{cart:id}/update',  [CartController::class, 'update'])->name('cart.update');

Route::delete('carts/{cart:id}/delete',  [CartController::class, 'destroy'])->name('cart.delete');

Route::get('carts/{cart:id}/products',  [CartController::class, 'showCartProducts'])->name('cart.products');

//Orders routes

Route::get('orders',[OrderController::class, 'index'])->name('order.index');

Route::get('orders/{order:id}',[OrderController::class, 'show'])->name('order.show');

Route::post('orders/create', [OrderController::class, 'store'])->name('order.create');

Route::put('orders/{cart:id}/update',  [OrderController::class, 'update'])->name('order.update');

Route::delete('orders/{cart:id}/delete',  [OrderController::class, 'destroy'])->name('order.delete');


